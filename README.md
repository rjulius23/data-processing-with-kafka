# Data processing with Kafka

Simple exercies to process data with kafka.

## Install

### Clone repo

 ```bash
 git clone https://gitlab.com/rjulius23/data-processing-with-kafka.git
 ```

### Set python path to repo

```bash
export PYTHONPATH=$PWD/data-processing-with-kafa:$PYTHONPATH
```

### Install python requirements

```bash
python3 -m pip install -r $PWD/data-processing-with-kafka/requirements.txt
```

## Run the data processor

### Open a new terminal to start data processor

```bash
python3 $PWD/data-processing-with-kafka/data_processor/service/processor.py
```

## Trigger the data importer

### Open a new terminal (dont close the one with the processor)

#### **Initialize database with baseline storage**

```bash
python3 $PWD/data-processing-with-kafka/data_processor/importer/scanner.py --csv path/to/data/csv_all_incoming.csv
```

#### **Feed in additional files**

```bash
for csv in $(ls path/to/data/csv_sample_*.csv); do python3 data_processor/importer/scanner.py --csv path/to/data/{$csv}; done
```

*NOTE: For errors during processing check processor's STDOUT.*