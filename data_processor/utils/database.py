import psycopg2
from psycopg2.extras import DictCursor
from typing import Dict


class DBCreds:
    """Class to store DB credentials."""

    def __init__(
        self,
        user: str = "testuser",
        password: str = "testpass123",
        host: str = "localhost",
        port: str = "5432",
        database: str = "exercise",
    ) -> None:
        """Init DB credentials."""
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.db = database


class DBHandler:
    """Class to handle PostgreSQL DB."""

    def __init__(self, db_creds: DBCreds):
        self.credentials = db_creds
        self.connection = psycopg2.connect(
            user=self.credentials.user,
            password=self.credentials.password,
            host=self.credentials.host,
            port=self.credentials.port,
            database=self.credentials.db,
        )

    def __del__(self):
        """Specify destructor for the class to close DB connection."""
        self.credentials = None
        if self.connection:
            self.connection.close()

    def create_tables(self):
        """Create storage and transactions table."""
        # At the moment these 2 tables are enough as we need the storage to store the
        # actual storage information and we also want to record the transactions so we can avoid double registering.
        # and also for historical reason, so it can be checked later what happened.
        commands = (
            """
            CREATE TABLE IF NOT EXISTS public.storage (
                store_number INTEGER NOT NULL,
                item_number INTEGER NOT NULL,
                value INTEGER,
                PRIMARY KEY (store_number, item_number)
            )
            """,
            """
            CREATE TABLE IF NOT EXISTS public.transactions (
                transaction_id TEXT PRIMARY KEY,
                event_type TEXT NOT NULL,
                store_number INTEGER,
                item_number INTEGER NOT NULL,
                date TEXT NOT NULL,
                value INTEGER
            )
            """,
        )
        cur = self.connection.cursor()
        # create table one by one
        for command in commands:
            cur.execute(command)
        cur.close()
        self.connection.commit()

    def check_if_exists_in_db(self, store_number: str, item_number: str):
        """Check whether provided store_number and item_number combo exists in DB.

        Args:
            store_number: store_number to be checked
            item_number: item_number to be checked

        Returns:
            Boolean
        """
        cur = self.connection.cursor()
        cur.execute(
            "SELECT store_number FROM storage WHERE store_number = %s AND item_number = %s",
            (store_number, item_number),
        )
        result = cur.fetchone() is not None
        cur.close()
        return result

    def check_if_transaction_exists_in_db(self, transaction_id: str):
        """Check whether provided transaction id exists in DB.

        Args:
            transaction_id: id to be checked in DB.

        Returns:
            Boolean
        """
        cur = self.connection.cursor()
        cur.execute(
            "SELECT transaction_id FROM transactions WHERE transaction_id = %(transaction_id)s",
            {"transaction_id": "{" + transaction_id + "}"},
        )
        result = cur.fetchone() is not None
        cur.close()
        return result

    def update_storage(self, data: Dict[str, str], table: str = "storage"):
        """Insert data into DB for storage table."""
        cur = self.connection.cursor()
        if not self.check_if_exists_in_db(data["store_number"], data["item_number"]):
            insert_command = f"""INSERT INTO {table} (store_number,item_number,value) VALUES (%(store_number)s, %(item_number)s, %(value)s)"""
            cur.execute(insert_command, data)
        else:
            update_command = f"""UPDATE {table} SET value = %(value)s WHERE store_number = %(store_number)s AND item_number = %(item_number)s"""
            cur.execute(update_command, data)
        cur.close()
        self.connection.commit()

    def save_transaction(
        self, event: Dict[str, str], table: str = "transactions"
    ) -> None:
        """Save transaction info to the DB for historical reasons.

        Args:
            event: dictionary with transaction info
        
        """
        cur = self.connection.cursor()
        insert_command = (
            f"""INSERT INTO {table}"""
            + """ (transaction_id,event_type,date,store_number,item_number,value) VALUES (%(transaction_id)s, %(event_type)s, %(date)s, %(store_number)s, %(item_number)s, %(value)s)"""
        )
        cur.execute(insert_command, event)

    def check_storage(self, data: Dict[str, str], table: str = "storage") -> int:
        """Check storage value for given item in store.

        Args:
            data: containing store and item info
        
        Returns:
            Current storage value.
        """
        cur = self.connection.cursor()
        if self.check_if_exists_in_db(data["store_number"], data["item_number"]):
            select = f"""SELECT value FROM {table} WHERE store_number = %(store_number)s AND item_number = %(item_number)s"""
            cur.execute(select, data)
            result = cur.fetchone()[0]
            if result < int(data["value"]):
                raise Exception(
                    f"Invalid sales event, sold more than available! Current {result} < Sold {data['value']}"
                )
        else:
            cur.close()
            raise Exception(
                f"Given item {data['item_number']} does not exist in store {data['store_number']}!"
            )

        cur.close()
        return result

    def read(self, table):
        select = f"SELECT * FROM {table}"
        cur = self.connection.cursor(cursor_factory=DictCursor)
        cur.execute(select)
        print(cur.fetchone())
        cur.close()


# For testing purposes
if __name__ == "__main__":
    creds = DBCreds()
    db = DBHandler(db_creds=creds)
    db.create_tables()
    db.update_storage(data={"store_number": 15, "item_number": 10, "value": 8})
    print(db.check_if_exists_in_db(store_number="15", item_number="10"))
    db.read(table="storage")
