import click
import json
from kafka import KafkaProducer, KafkaConsumer
import sys
from typing import Dict, Generator, Optional


class EventProducer(KafkaProducer):
    """Class to produce events."""

    def __init__(
        self, bootstrap_servers: str = "localhost:9092", topic: str = "events", **kwargs
    ) -> None:
        """Initialize specialized KafkaProducer.

        Args:
            bootstrap_servers: Possible to specifiy bootstrap servers.
            topic: Topic to publish to.
        """
        self.topic = topic
        super().__init__(bootstrap_servers=bootstrap_servers, **kwargs)

    def publish(self, payload: Dict[str, str]) -> None:
        """Send to class's topic the payload.

        Args:
            payload: Data to publish to Kafka.
        """
        self.send(self.topic, payload)


class EventHandler:
    """Class to consume events from Kafka Producer."""

    @property
    def producer(self):
        """Getter for producer property."""
        return self.__producer

    @producer.setter
    def producer(self, producer: EventProducer):
        """Set producer."""
        self.__producer = producer

    @property
    def consumer(self):
        """Getter for consumer property."""
        return self.__consumer

    @consumer.setter
    def consumer(self, consumer: KafkaConsumer):
        """Set consumer."""
        self.__consumer = consumer

    def __init__(
        self,
        producer: EventProducer = None,
        consumer: KafkaConsumer = None,
        topic: str = "events",
    ):
        """Init event handler class.

        Args:
            producer: EventProducer instance if provided
            consumer: KafkaConsumer instance if provided
            topic: default topic value is events
        """
        self.__producer = (
            producer
            if producer
            else EventProducer(value_serializer=lambda v: json.dumps(v).encode("utf-8"))
        )
        self.__consumer = consumer if consumer else KafkaConsumer(topic)

    def publish_on_event(self, data: Dict[str, str]):
        """Publish on event.

        Args:
            data: payload to publish
        """
        self.__producer.publish(data)

    def consume_loop(self) -> Generator[str, None, None]:
        """Start loop for consuming events."""
        for event in self.__consumer:
            yield json.loads(event.value.decode("utf-8"))
        return None


# For testing purpusos
@click.command()
@click.option("--consume", is_flag=True, help="Start consumer loop.")
@click.option("--topic", default="events", help="Specify topic to listen to.")
def main(consume: bool, topic: str):
    if topic:
        event_handler = EventHandler(topic=topic)
    else:
        event_handler = EventHandler()

    if consume:
        for value in event_handler.consume_loop():
            print(value)
    return 0


if __name__ == "__main__":
    sys.exit(main())
