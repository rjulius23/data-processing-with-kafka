from data_processor.importer.scanner import CsvScanner as importer
from data_processor.importer.scanner import InvalidValueError
from data_processor.utils.database import DBCreds, DBHandler
from data_processor.utils.event_handler import EventHandler
import logging
from typing import Dict

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.INFO)


class DataProcessor:
    """Class to handle incomng and sales events from importer."""

    def __init__(self, eh: EventHandler = None, db_creds: DBCreds = None) -> None:
        """Initialize class.

        Args:
            eh: EventHandler instance, optional

        """
        self.creds = db_creds if db_creds else DBCreds()
        self.db = DBHandler(db_creds=db_creds)
        self.eh = eh if eh else EventHandler(topic="events")

    def handle_event_loop(self) -> None:
        """Loop to handle events coming from Kafka producers."""
        for event in self.eh.consume_loop():
            self.process_event(event)

    @staticmethod
    def validate(event: Dict[str, str]) -> bool:
        """Validate event against the schema.

        Args:
            event: Dictionary containing event payload and data

        Returns:
            True if valid , False otherwise.
        """
        result = True
        try:
            importer.validate(event)
        except InvalidValueError as exc:
            LOG.warning(f"Invalid values in the event payload! {exc}")
            result = False
        return result

    def handle_incoming(self, event: Dict[str, str]) -> None:
        """Handle incoming events (addition of items).

        Args:
            event: Dictionary that contains required information.

        """
        LOG.info("Updating storage with incoming event...")
        self.db.update_storage(data=event)

    def handle_sales(self, event: Dict[str, str]) -> None:
        """Handle sales events (selling from storage)

        Args:
            event: Dictionary that contains required information.

        """
        try:
            current_value = self.db.check_storage(data=event)
        except Exception as exc:
            LOG.warning(
                f"Failed to check storage for event: {event}, Details: {str(exc)}"
            )
        else:
            try:
                event["value"] = str(current_value - int(event["value"]))
                LOG.info("Updating storage with sales event...")
                self.db.update_storage(data=event)
            except Exception as exc:
                LOG.error(
                    f"Failed to update storage during transaction {event['transaction_id']} due to {str(exc)}"
                )

    def push_data_to_db(self, data: Dict[str, str]) -> None:
        """Convert data values do some checks and push to DB.

        Args:
            data: dictionary containing data

        """
        event_mapping = {"incoming": self.handle_incoming, "sale": self.handle_sales}
        event_type = data["event_type"]
        transaction_id = data["transaction_id"]

        if self.db.check_if_transaction_exists_in_db(transaction_id=transaction_id):
            LOG.warning(
                f"Transaction({transaction_id}) has been processed earlier already...Skipping!"
            )
        else:
            try:
                self.db.save_transaction(event=data)
                event_mapping[event_type](event=data)
            except Exception as exc:
                LOG.error(
                    f"Failed to handle specific event ({event_type}), due to {exc}...!"
                )
                raise

    def process_event(self, event: Dict[str, str]) -> None:
        """Process events coming from producer.

        Args:
            event: Dictionary containing event payload and data

        """
        if self.validate(event):
            LOG.info("Valid event has been received...")
            self.push_data_to_db(event)
        else:
            LOG.warning(f"The following event was skipped due to malformation: {event}")


if __name__ == "__main__":
    db_creds = DBCreds()
    dh = DBHandler(db_creds=db_creds)
    dh.create_tables()
    dp = DataProcessor(db_creds=db_creds)
    dp.handle_event_loop()
