import click
from collections import OrderedDict
import csv
import dask.dataframe as ddf
import logging
from pathlib import Path
from typing import Any, Dict, Generator, List
import json
from jsonschema import validate

from data_processor.utils.event_handler import EventHandler

LOG = logging.getLogger(__name__)
# logging.basicConfig(filename="importer.log", level=logging.INFO)


class InvalidValueError(Exception):
    """Exception raised for errors when json validation fails.

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message: str):
        self.message = message
        super().__init__(self.message)


class CsvScanner:
    """Class to scan CSV files and process them line by line."""

    dataSchema = {
        "properties": {
            "transaction_id": {"type": "string"},
            "event_type": {"type": "string", "pattern": "^(incoming|sale)$"},
            "date": {
                "type": "string",
                "pattern": "^20[0-9][0-9]-[0-1][0-9]-[0-9]+T[0-2][0-9]:[0-5][0-9]:[0-5][0-9]Z$",
            },
            "store_number": {"type": "string", "pattern": "^[0-9]+$"},
            "item_number": {"type": "string", "pattern": "^[0-9]+$"},
            "value": {"type": "string", "pattern": "^[0-9]+$"},
        },
    }

    def __init__(self, csv_path: Path) -> None:
        """Init class."""
        self.csv = csv_path

    def open_csv(self):
        """Open CSV with dask and return DataFrame."""
        try:
            with self.csv.open("r") as f:
                reader = csv.DictReader(
                    f
                )  # ddf.read_csv(self.csv, parse_dates=['date'])
                for row in reader:
                    yield row
        except Exception:
            LOG.error(f"Failed to open CSV File: {str(self.csv)}")
            raise

    @staticmethod
    def validate(data: Dict[str, Any]) -> None:
        """Validate date according to schema.

        Args:
            data: Dictionary to validate.

        Raises:
            Invalida Data exception.
        """
        try:
            validate(instance=data, schema=CsvScanner.dataSchema)
        except Exception as exc:
            LOG.warning("Validation failed!")
            raise InvalidValueError(str(exc))

    @staticmethod
    def convert_to_json(data: OrderedDict) -> Dict[str, str]:
        """Convert from OrderedDict to Dict."""
        return json.loads(json.dumps(data))

    def process_data(self) -> None:
        """Process CSV data line by line."""
        csv_data = self.open_csv()
        eh = EventHandler(topic="events")
        for row in csv_data:
            json_payload = self.convert_to_json(row)
            try:
                self.validate(json_payload)
            except InvalidValueError as exc:
                LOG.warning(f"Validation failed for {row} with {str(exc)}!")
            else:
                # Only publish valid data
                eh.publish_on_event(json_payload)


@click.command()
@click.option("--csv", required=True, help="Path to CSV file to process.")
def main(csv: str):
    scanner = CsvScanner(csv_path=Path(csv))
    scanner.process_data()


if __name__ == "__main__":
    main()
